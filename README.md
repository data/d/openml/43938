# OpenML dataset: nursery

https://www.openml.org/d/43938

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Nursery Database was derived from a hierarchical decision model originally developed to rank applications for nursery schools.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43938) of an [OpenML dataset](https://www.openml.org/d/43938). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43938/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43938/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43938/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

